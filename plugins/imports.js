module.exports = {
	env: {
		es6: true,
	},
	parserOptions: {
		ecmaVersion: 6,
		sourceType: 'module',
	},
	plugins: [
		'import',
	],

	settings: {
		'import/resolver': {
			node: {
				extensions: ['.js', '.json'],
			},
		},
		'import/extensions': [
			'.js',
			'.jsx',
		],
		'import/core-modules': [
		],
		'import/ignore': [
			'node_modules',
			'\\.(coffee|scss|css|less|hbs|svg|json)$',
		],
	},

	rules: {
		// Static analysis:

		// Ensure imports point to files/modules that can be resolved
		// Turn this off because we use bower
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-unresolved.md
		'import/no-unresolved': 'off',

		// ensure named imports coupled with named exports
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/named.md#when-not-to-use-it
		'import/named': 'off',

		// ensure default import coupled with default export
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/default.md#when-not-to-use-it
		'import/default': 'off',

		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/namespace.md
		'import/namespace': 'off',

		// Restrict which files can be imported in a given folder
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-restricted-paths.md
		'import/no-restricted-paths': 'off',

		// Forbid import of modules using absolute paths
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-absolute-path.md
		'import/no-absolute-path': 'error',

		// Forbid require() calls with expressions
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-dynamic-require.md
		'import/no-dynamic-require': 'off',

		// Prevent importing the submodules of other modules
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-internal-modules.md
		'import/no-internal-modules': ['off', {
			allow: [],
		}],

		// Forbid Webpack loader syntax in imports
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-webpack-loader-syntax.md
		'import/no-webpack-loader-syntax': 'error',

		// Helpful warnings:

		// disallow invalid exports, e.g. multiple defaults
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/export.md
		'import/export': 'error',

		// do not allow a default import name to match a named export
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-named-as-default.md
		'import/no-named-as-default': 'error',

		// warn on accessing default export property names that are also named exports
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-named-as-default-member.md
		'import/no-named-as-default-member': 'error',

		// disallow use of jsdoc-marked-deprecated imports
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-deprecated.md
		'import/no-deprecated': 'off',

		// Forbid the use of extraneous packages
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-extraneous-dependencies.md
		'import/no-extraneous-dependencies': 'off',

		// Forbid mutable exports
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-mutable-exports.md
		'import/no-mutable-exports': 'error',

		// Module systems:

		// Warn if a module could be mistakenly parsed as a script by a consumer
		// leveraging Unambiguous JavaScript Grammar
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/unambiguous.md
		// this should not be enabled until this proposal has at least been *presented* to TC39.
		// At the moment, it"s not a thing.
		'import/unambiguous': 'off',

		// disallow require()
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-commonjs.md
		'import/no-commonjs': 'off',

		// disallow AMD require/define
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-amd.md
		'import/no-amd': 'error',

		// No Node.js builtin modules
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-nodejs-modules.md
		'import/no-nodejs-modules': 'off',

		// Style guide:

		// disallow non-import statements appearing before import statements
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/first.md
		'import/first': ['error', 'absolute-first'],

		// disallow duplicate imports
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-duplicates.md
		'import/no-duplicates': ['error', { considerQueryString: true }],

		// disallow namespace imports
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-namespace.md
		'import/no-namespace': 'off',

		// Ensure consistent use of file extension within the import path
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/extensions.md
		'import/extensions': ['error', 'always', {
			js: 'never',
			jsx: 'never',
		}],

		// Enforce a convention in module import order
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/order.md
		'import/order': ['off', {
			groups: ['builtin', 'external', 'internal', 'parent', 'sibling', 'index'],
			'newlines-between': 'never',
		}],

		// Require a newline after the last import/require in a group
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/newline-after-import.md
		'import/newline-after-import': 'error',

		// Require modules with a single export to use a default export
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/prefer-default-export.md
		'import/prefer-default-export': 'error',

		// Forbid modules to have too many dependencies
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/max-dependencies.md
		'import/max-dependencies': ['off', { max: 10 }],

		// Prevent unassigned imports
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-unassigned-import.md
		// importing for side effects is perfectly acceptable, if you need side effects.
		'import/no-unassigned-import': 'off',

		// Prevent importing the default as if it were named
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-named-default.md
		'import/no-named-default': 'error',

		// Forbid anonymous values as default exports
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-anonymous-default-export.md
		'import/no-anonymous-default-export': 'off',

		// This rule enforces that all exports are declared at the bottom of the file.
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/exports-last.md
		'import/exports-last': 'off',

		// Reports when named exports are not grouped together in a single export declaration
		// or when multiple assignments to CommonJS module.exports or exports object are present
		// in a single file.
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/group-exports.md
		'import/group-exports': 'off',

		// Forbid default exports.
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-default-export.md
		'import/no-default-export': 'off',

		// Prohibit named exports. this is a terrible rule, do not use it.
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-named-export.md
		'import/no-named-export': 'off',

		// Forbid a module from importing itself
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-self-import.md
		'import/no-self-import': 'error',

		// Forbid cyclical dependencies between modules
		// This rule is comparatively computationally expensive, don't enable it before we make sure no performance issue on everyone.
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-cycle.md
		'import/no-cycle': ['off', { maxDepth: Infinity }],

		// Ensures that there are no useless path segments
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-useless-path-segments.md
		'import/no-useless-path-segments': 'off',

		// Use this rule to prevent imports to folders in relative parent paths.
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-relative-parent-imports.md
		'import/no-relative-parent-imports': 'off',

		// Reports modules without any exports, or with unused exports
		// https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-unused-modules.md
		'import/no-unused-modules': 'off',
	},
};
