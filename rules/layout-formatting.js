// https://eslint.org/docs/latest/rules/#layout--formatting
module.exports = {
	rules: {
		// enforce position of line comments
		// http://eslint.org/docs/rules/line-comment-position
		'line-comment-position': ['off', {
			position: 'above',
			ignorePattern: '',
			applyDefaultIgnorePatterns: true,
		}],

		// require or disallow the Unicode Byte Order Mark
		// http://eslint.org/docs/rules/unicode-bom
		'unicode-bom': ['error', 'never'],
	},
};
