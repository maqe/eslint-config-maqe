// https://eslint.org/docs/latest/rules/#suggestions
module.exports = {
	rules: {
		// enforces getter/setter pairs in objects
		// https://eslint.org/docs/latest/rules/accessor-pairs
		'accessor-pairs': 'off',

		// enforces no braces where they can be omitted
		// http://eslint.org/docs/rules/arrow-body-style
		'arrow-body-style': ['off', 'as-needed', {
			requireReturnForObjectLiteral: false,
		}],

		// treat var statements as if they were block scoped
		// https://eslint.org/docs/latest/rules/block-scoped-var
		'block-scoped-var': 'off',

		// require camel case names
		// https://eslint.org/docs/latest/rules/camelcase
		camelcase: ['error', { properties: 'never' }],

		// enforce or disallow capitalization of the first letter of a comment
		// http://eslint.org/docs/rules/capitalized-comments
		'capitalized-comments': ['off', 'never', {
			line: {
				ignorePattern: '.*',
				ignoreInlineComments: true,
				ignoreConsecutiveComments: true,
			},
			block: {
				ignorePattern: '.*',
				ignoreInlineComments: true,
				ignoreConsecutiveComments: true,
			},
		}],

		// enforce that class methods use "this"
		// http://eslint.org/docs/rules/class-methods-use-this
		'class-methods-use-this': 'off',

		// specify the maximum cyclomatic complexity allowed in a program
		// https://eslint.org/docs/latest/rules/complexity
		complexity: ['off', 11],

		// require return statements to either always or never specify values
		// https://eslint.org/docs/latest/rules/consistent-return
		'consistent-return': 'error',

		// enforces consistent naming when capturing the current execution context
		// https://eslint.org/docs/latest/rules/consistent-this
		'consistent-this': 'off',

		// specify curly brace conventions for all control statements
		// https://eslint.org/docs/latest/rules/curly
		curly: ['error', 'multi-line'],

		// require default case in switch statements
		// https://eslint.org/docs/latest/rules/default-case
		'default-case': ['error', {
			commentPattern: '^no default$',
		}],

		// enforce default clauses in switch statements to be last
		// https://eslint.org/docs/rules/default-case-last
		'default-case-last': 'error',

		// enforce default parameters to be last
		// https://eslint.org/docs/rules/default-param-last
		'default-param-last': 'warn',

		// encourages use of dot notation whenever possible
		// https://eslint.org/docs/latest/rules/dot-notation
		'dot-notation': ['error', { allowKeywords: true }],

		// require the use of === and !==
		// http://eslint.org/docs/rules/eqeqeq
		eqeqeq: ['warn', 'smart'],

		// requires function names to match the name of the variable or property to which they are
		// assigned
		// http://eslint.org/docs/rules/func-name-matching
		'func-name-matching': ['off', 'always', {
			includeCommonJSModuleExports: false,
		}],

		// require function expressions to have a name
		// http://eslint.org/docs/rules/func-names
		'func-names': 'warn',

		// enforces use of function declarations or expressions
		// http://eslint.org/docs/rules/func-style
		'func-style': ['off', 'expression'],

		// require grouped accessor pairs in object literals and classes
		// https://eslint.org/docs/rules/grouped-accessor-pairs
		'grouped-accessor-pairs': ['error', 'getBeforeSet'],

		// make sure for-in loops have an if statement
		// https://eslint.org/docs/latest/rules/guard-for-in
		'guard-for-in': 'error',

		// Disallow specified identifiers
		// https://eslint.org/docs/latest/rules/id-denylist
		'id-denylist': 'off',

		// this option enforces minimum and maximum identifier lengths
		// (variable names, property names etc.)
		// https://eslint.org/docs/latest/rules/id-length
		'id-length': 'off',

		// require identifiers to match the provided regular expression
		// https://eslint.org/docs/latest/rules/id-match
		'id-match': 'off',

		// enforce or disallow variable initializations at definition
		// https://eslint.org/docs/latest/rules/init-declarations
		'init-declarations': 'off',

		// Require or disallow logical assignment logical operator shorthand
		// https://eslint.org/docs/latest/rules/logical-assignment-operators
		'logical-assignment-operators': ['warn', 'always'],

		// enforce a maximum number of classes per file
		// https://eslint.org/docs/rules/max-classes-per-file
		'max-classes-per-file': ['error', 1],

		// specify the maximum depth that blocks can be nested
		// https://eslint.org/docs/latest/rules/max-depth
		'max-depth': ['off', 4],

		// specify the max number of lines in a file
		// http://eslint.org/docs/rules/max-lines
		'max-lines': ['off', {
			max: 300,
			skipBlankLines: true,
			skipComments: true,
		}],

		// enforce a maximum function length
		// https://eslint.org/docs/rules/max-lines-per-function
		'max-lines-per-function': 'off',

		// specify the maximum depth callbacks can be nested
		// https://eslint.org/docs/latest/rules/max-nested-callbacks
		'max-nested-callbacks': 'off',

		// limits the number of parameters that can be used in the function declaration.
		// https://eslint.org/docs/latest/rules/max-params
		'max-params': ['off', 3],

		// specify the maximum number of statement allowed in a function
		// https://eslint.org/docs/latest/rules/max-statements
		'max-statements': ['off', 10],

		// enforce a particular style for multiline comments
		// https://eslint.org/docs/rules/multiline-comment-style
		'multiline-comment-style': ['off', 'starred-block'],

		// require a capital letter for constructors
		// https://eslint.org/docs/latest/rules/new-cap
		'new-cap': ['error', {
			newIsCap: true,
			newIsCapExceptions: [],
			capIsNew: false,
			capIsNewExceptions: [
				'Immutable.Map',
				'Immutable.Set',
				'Immutable.List',
			],
		}],

		// disallow the use of alert, confirm, and prompt
		// https://eslint.org/docs/latest/rules/no-alert
		'no-alert': 'warn',

		// disallow use of the Array constructor
		// https://eslint.org/docs/latest/rules/no-array-constructor
		'no-array-constructor': 'error',

		// disallow use of bitwise operators
		// http://eslint.org/docs/rules/no-bitwise
		'no-bitwise': ['error', { allow: ['~'] }],

		// disallow use of arguments.caller or arguments.callee
		// https://eslint.org/docs/latest/rules/no-caller
		'no-caller': 'error',

		// disallow lexical declarations in case/default clauses
		// http://eslint.org/docs/rules/no-case-declarations.html
		'no-case-declarations': 'error',

		// disallow use of console
		// https://eslint.org/docs/latest/rules/no-console
		'no-console': 'warn',

		// disallow use of the continue statement
		// http://eslint.org/docs/rules/no-continue
		'no-continue': 'error',

		// disallow deletion of variables
		// https://eslint.org/docs/latest/rules/no-delete-var
		'no-delete-var': 'error',

		// disallow division operators explicitly at beginning of regular expression
		// http://eslint.org/docs/rules/no-div-regex
		'no-div-regex': 'off',

		// disallow else after a return in an if
		// https://eslint.org/docs/latest/rules/no-else-return
		'no-else-return': 'error',

		// disallow empty statements
		// https://eslint.org/docs/latest/rules/no-empty
		'no-empty': 'error',

		// disallow empty functions, except for standalone funcs/arrows
		// http://eslint.org/docs/rules/no-empty-function
		'no-empty-function': ['error', {
			allow: [
				'arrowFunctions',
				'functions',
				'methods',
			],
		}],

		// Disallow empty static blocks
		// https://eslint.org/docs/latest/rules/no-empty-static-block
		'no-empty-static-block': 'off',

		// disallow comparisons to null without a type-checking operator
		// https://eslint.org/docs/latest/rules/no-eq-null
		'no-eq-null': 'off',

		// disallow use of eval()
		// https://eslint.org/docs/latest/rules/no-eval
		'no-eval': 'error',

		// disallow adding to native types
		// https://eslint.org/docs/latest/rules/no-extend-native
		'no-extend-native': 'error',

		// disallow unnecessary function binding
		// https://eslint.org/docs/latest/rules/no-extra-bind
		'no-extra-bind': 'error',

		// disallow double-negation boolean casts in a boolean context
		// http://eslint.org/docs/rules/no-extra-boolean-cast
		'no-extra-boolean-cast': 'error',

		// disallow Unnecessary Labels
		// http://eslint.org/docs/rules/no-extra-label
		'no-extra-label': 'error',

		// disallow reassignments of native objects or read-only globals
		// http://eslint.org/docs/rules/no-global-assign
		'no-global-assign': ['error', { exceptions: [] }],

		// disallow implicit type conversions
		// http://eslint.org/docs/rules/no-implicit-coercion
		'no-implicit-coercion': ['off', {
			boolean: false,
			number: true,
			string: true,
			allow: [],
		}],

		// disallow var and named functions in global scope
		// http://eslint.org/docs/rules/no-implicit-globals
		'no-implicit-globals': 'off',

		// disallow use of eval()-like methods
		// https://eslint.org/docs/latest/rules/no-implied-eval
		'no-implied-eval': 'error',

		// disallow comments inline after code
		// https://eslint.org/docs/latest/rules/no-inline-comments
		'no-inline-comments': 'off',

		// disallow this keywords outside of classes or class-like objects
		// https://eslint.org/docs/latest/rules/no-invalid-this
		'no-invalid-this': 'off',

		// disallow usage of __iterator__ property
		// https://eslint.org/docs/latest/rules/no-iterator
		'no-iterator': 'error',

		// disallow labels that share a name with a variable
		// http://eslint.org/docs/rules/no-label-var
		'no-label-var': 'error',

		// disallow use of labels for anything other then loops and switches
		// https://eslint.org/docs/latest/rules/no-labels
		'no-labels': ['error', {
			allowLoop: false,
			allowSwitch: false,
		}],

		// disallow unnecessary nested blocks
		// https://eslint.org/docs/latest/rules/no-lone-blocks
		'no-lone-blocks': 'error',

		// disallow if as the only statement in an else block
		// http://eslint.org/docs/rules/no-lonely-if
		'no-lonely-if': 'error',

		// disallow creation of functions within loops
		// https://eslint.org/docs/latest/rules/no-loop-func
		'no-loop-func': 'error',

		// disallow magic numbers
		// http://eslint.org/docs/rules/no-magic-numbers
		'no-magic-numbers': ['off', {
			ignore: [],
			ignoreArrayIndexes: true,
			enforceConst: true,
			detectObjects: false,
		}],

		// disallow use of chained assignment expressions
		// http://eslint.org/docs/rules/no-multi-assign
		'no-multi-assign': 'off',

		// disallow use of multiline strings
		// https://eslint.org/docs/latest/rules/no-multi-str
		'no-multi-str': 'error',

		// disallow negated conditions
		// http://eslint.org/docs/rules/no-negated-condition
		'no-negated-condition': 'off',

		// disallow nested ternary expressions
		// https://eslint.org/docs/latest/rules/no-nested-ternary
		'no-nested-ternary': 'error',

		// disallow use of new operator when not part of the assignment or comparison
		// https://eslint.org/docs/latest/rules/no-new
		'no-new': 'error',

		// disallow use of new operator for Function object
		// https://eslint.org/docs/latest/rules/no-new-func
		'no-new-func': 'error',

		// Disallow calls to the Object constructor without an argument
		// https://eslint.org/docs/latest/rules/no-object-constructor
		'no-object-constructor': 'error',

		// disallows creating new instances of String, Number, and Boolean
		// https://eslint.org/docs/latest/rules/no-new-wrappers
		'no-new-wrappers': 'error',

		// disallow `\8` and `\9` escape sequences in string literals
		// https://eslint.org/docs/rules/no-nonoctal-decimal-escape
		'no-nonoctal-decimal-escape': 'error',

		// disallow use of (old style) octal literals
		// https://eslint.org/docs/latest/rules/no-octal
		'no-octal': 'error',

		// disallow use of octal escape sequences in string literals, such as
		// var foo = "Copyright \251";
		// https://eslint.org/docs/latest/rules/no-octal-escape
		'no-octal-escape': 'error',

		// disallow reassignment of function parameters
		// disallow parameter object manipulation
		// rule: http://eslint.org/docs/rules/no-param-reassign.html
		'no-param-reassign': ['error', { props: false }],

		// disallow use of unary operators, ++ and --
		// http://eslint.org/docs/rules/no-plusplus
		'no-plusplus': 'off',

		// disallow usage of __proto__ property
		// http://eslint.org/docs/rules/no-proto
		'no-proto': 'error',

		// disallow declaring the same variable more then once
		// https://eslint.org/docs/latest/rules/no-redeclare
		'no-redeclare': ['error', { builtinGlobals: true }],

		// disallow multiple spaces in a regular expression literal
		// https://eslint.org/docs/latest/rules/no-regex-spaces
		'no-regex-spaces': 'error',

		// disallow specified names in exports
		// https://eslint.org/docs/rules/no-restricted-exports
		'no-restricted-exports': 'off',

		// disallow specific globals
		// https://eslint.org/docs/latest/rules/no-restricted-globals
		'no-restricted-globals': 'off',

		// disallow specific imports
		// http://eslint.org/docs/rules/no-restricted-imports
		'no-restricted-imports': 'off',

		// disallow certain object properties
		// http://eslint.org/docs/rules/no-restricted-properties
		'no-restricted-properties': ['error', {
			object: 'arguments',
			property: 'callee',
			message: 'arguments.callee is deprecated',
		}, {
			property: '__defineGetter__',
			message: 'Please use Object.defineProperty instead.',
		}, {
			property: '__defineSetter__',
			message: 'Please use Object.defineProperty instead.',
		}, {
			object: 'Math',
			property: 'pow',
			message: 'Use the exponentiation operator (**) instead.',
		}],

		// disallow certain syntax forms
		// http://eslint.org/docs/rules/no-restricted-syntax
		'no-restricted-syntax': [
			'error',
			'LabeledStatement',
			'WithStatement',
		],

		// disallow use of assignment in return statement
		// https://eslint.org/docs/latest/rules/no-return-assign
		'no-return-assign': 'error',

		// disallow use of `javascript:` urls.
		// https://eslint.org/docs/latest/rules/no-script-url
		'no-script-url': 'error',

		// disallow use of comma operator
		// https://eslint.org/docs/latest/rules/no-sequences
		'no-sequences': ['error', { allowInParentheses: true }],

		// disallow declaration of variables already declared in the outer scope
		// https://eslint.org/docs/latest/rules/no-shadow
		'no-shadow': ['error', { ignoreOnInitialization: false }],

		// disallow shadowing of names such as arguments
		// https://eslint.org/docs/latest/rules/no-shadow-restricted-names
		'no-shadow-restricted-names': 'error',

		// disallow the use of ternary operators
		// https://eslint.org/docs/latest/rules/no-ternary
		'no-ternary': 'off',

		// restrict what can be thrown as an exception
		// https://eslint.org/docs/latest/rules/no-throw-literal
		'no-throw-literal': 'error',

		// disallow use of undefined when initializing variables
		// https://eslint.org/docs/latest/rules/no-undef-init
		'no-undef-init': 'error',

		// disallow use of undefined variable
		// http://eslint.org/docs/rules/no-undefined
		'no-undefined': 'off',

		// disallow dangling underscores in identifiers
		// https://eslint.org/docs/latest/rules/no-underscore-dangle
		'no-underscore-dangle': 'off',

		// disallow the use of Boolean literals in conditional expressions
		// also, prefer `a || b` over `a ? a : b`
		// http://eslint.org/docs/rules/no-unneeded-ternary
		'no-unneeded-ternary': ['error', { defaultAssignment: false }],

		// disallow usage of expressions in statement position
		// https://eslint.org/docs/latest/rules/no-unused-expressions
		'no-unused-expressions': ['error', {
			allowShortCircuit: false,
			allowTernary: false,
			enforceForJSX: false,
		}],

		// disallow unused labels
		// http://eslint.org/docs/rules/no-unused-labels
		'no-unused-labels': 'error',

		// disallow unnecessary .call() and .apply()
		// https://eslint.org/docs/latest/rules/no-useless-call
		'no-useless-call': 'off',

		// Disallow unnecessary catch clauses
		// https://eslint.org/docs/rules/no-useless-catch
		'no-useless-catch': 'error',

		// disallow useless computed property keys
		// http://eslint.org/docs/rules/no-useless-computed-key
		'no-useless-computed-key': 'error',

		// disallow useless string concatenation
		// http://eslint.org/docs/rules/no-useless-concat
		'no-useless-concat': 'error',

		// disallow unnecessary constructor
		// http://eslint.org/docs/rules/no-useless-constructor
		'no-useless-constructor': 'error',

		// disallow unnecessary string escaping
		// http://eslint.org/docs/rules/no-useless-escape
		'no-useless-escape': 'error',

		// disallow renaming import, export, and destructured assignments to the same name
		// http://eslint.org/docs/rules/no-useless-rename
		'no-useless-rename': ['error', {
			ignoreDestructuring: false,
			ignoreImport: false,
			ignoreExport: false,
		}],

		// disallow redundant return; keywords
		// http://eslint.org/docs/rules/no-useless-return
		'no-useless-return': 'error',

		// require let or const instead of var
		// https://eslint.org/docs/latest/rules/no-var
		'no-var': 'error',

		// disallow use of void operator
		// http://eslint.org/docs/rules/no-void
		'no-void': 'error',

		// disallow usage of configurable warning terms in comments: e.g. todo
		// https://eslint.org/docs/latest/rules/no-warning-comments
		'no-warning-comments': ['off', {
			terms: ['todo', 'fixme', 'xxx'],
			location: 'start',
		}],

		// disallow use of the with statement
		// https://eslint.org/docs/latest/rules/no-with
		'no-with': 'error',

		// require method and property shorthand syntax for object literals
		// http://eslint.org/docs/rules/object-shorthand
		'object-shorthand': ['error', 'always', {
			ignoreConstructors: false,
			avoidQuotes: true,
		}],

		// allow just one var statement per function
		// https://eslint.org/docs/latest/rules/one-var
		'one-var': 'off',

		// require assignment operator shorthand where possible or prohibit it entirely
		// http://eslint.org/docs/rules/operator-assignment
		'operator-assignment': ['error', 'always'],

		// suggest using arrow functions as callbacks
		// https://eslint.org/docs/latest/rules/prefer-arrow-callback
		'prefer-arrow-callback': ['error', {
			allowNamedFunctions: false,
			allowUnboundThis: true,
		}],

		// suggest using of const declaration for variables that are never modified after declared
		// https://eslint.org/docs/latest/rules/prefer-const
		'prefer-const': 'warn',

		// Prefer destructuring from arrays and objects
		// http://eslint.org/docs/rules/prefer-destructuring
		'prefer-destructuring': ['off', {
			array: true,
			object: true,
		}, {
			enforceForRenamedProperties: false,
		}],

		// disallow the use of `Math.pow` in favor of the `**` operator
		// https://eslint.org/docs/rules/prefer-exponentiation-operator
		'prefer-exponentiation-operator': 'off',

		// Suggest using named capture group in regular expression
		// https://eslint.org/docs/rules/prefer-named-capture-group
		'prefer-named-capture-group': 'off',

		// disallow parseInt() in favor of binary, octal, and hexadecimal literals
		// http://eslint.org/docs/rules/prefer-numeric-literals
		'prefer-numeric-literals': 'error',

		// disallow use of `Object.prototype.hasOwnProperty.call()` and prefer use of `Object.hasOwn()`
		// note: set error after upgrade ecmascript to 2022
		// https://eslint.org/docs/rules/prefer-object-has-own
		'prefer-object-has-own': 'off',

		// Prefer use of an object spread over Object.assign
		// https://eslint.org/docs/rules/prefer-object-spread
		'prefer-object-spread': 'error',

		// require using Error objects as Promise rejection reasons
		// http://eslint.org/docs/rules/prefer-promise-reject-errors
		'prefer-promise-reject-errors': ['error', {
			allowEmptyReject: true,
		}],

		// disallow use of the `RegExp` constructor in favor of regular expression literals
		// https://eslint.org/docs/rules/prefer-regex-literals
		'prefer-regex-literals': ['error', {
			disallowRedundantWrapping: true,
		}],

		// use rest parameters instead of arguments
		// http://eslint.org/docs/rules/prefer-rest-params
		'prefer-rest-params': 'error',

		// suggest using the spread operator instead of .apply()
		// http://eslint.org/docs/rules/prefer-spread
		'prefer-spread': 'error',

		// suggest using template literals instead of string concatenation
		// http://eslint.org/docs/rules/prefer-template
		'prefer-template': 'error',

		// require use of the second argument for parseInt()
		// https://eslint.org/docs/latest/rules/radix
		radix: 'error',

		// require `await` in `async function` (note: this is a horrible rule that should never be used)
		// http://eslint.org/docs/rules/require-await
		'require-await': 'off',

		// Enforce the use of u flag on RegExp
		// https://eslint.org/docs/rules/require-unicode-regexp
		'require-unicode-regexp': 'off',

		// disallow generator functions that do not have yield
		// http://eslint.org/docs/rules/require-yield
		'require-yield': 'error',

		// import sorting
		// http://eslint.org/docs/rules/sort-imports
		'sort-imports': ['off', {
			ignoreCase: false,
			ignoreMemberSort: false,
			memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single'],
		}],

		// requires object keys to be sorted
		// https://eslint.org/docs/latest/rules/sort-keys
		'sort-keys': ['off', 'asc', {
			caseSensitive: false,
			natural: true,
		}],

		// sort variables within the same declaration block
		// https://eslint.org/docs/latest/rules/sort-vars
		'sort-vars': 'off',

		// babel inserts `"use strict";` for us
		// https://eslint.org/docs/latest/rules/strict
		strict: ['error', 'never'],

		// require a Symbol description
		// http://eslint.org/docs/rules/symbol-description
		'symbol-description': 'error',

		// requires to declare all vars on top of their containing scope
		// https://eslint.org/docs/latest/rules/vars-on-top
		'vars-on-top': 'warn',

		// require or disallow Yoda conditions
		// https://eslint.org/docs/latest/rules/yoda
		yoda: 'error',
	},
};
