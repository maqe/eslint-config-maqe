module.exports = {
	extends: [
		'./rules/possible-problems.js',
		'./rules/suggestions.js',
		'./rules/layout-formatting.js',

		'./plugins/node.js',
		'./plugins/imports.js',
		'./plugins/eslint-stylistic.js',
	],
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module',
		ecmaFeatures: {
			generators: false,
			objectLiteralDuplicateProperties: false,
		},
	},
	globals: {
		$: 'writeable',
	},
	env: {
		browser: true,
		node: true,
		es6: true,
	},
};
