## v6.2.0 (2024-07-10):

- Change `allowSamePrecedence` option to `true` for `@stylistic/no-mixed-operators` rule

## v6.1.0 (2024-01-08):

- Switch from `@stylistic/eslint-plugin-js` to `@stylistic/eslint-plugin` package

## v6.0.0 (2024-01-04):

- Bump `eslint` dependency to >= 8.56.0
- Bump `eslint-plugin-import` dependency to 2.29.1
- Install a new plugin: `@stylistic/eslint-plugin-js` v1.5.3
- [Migrate deprecated stylistic rules from core `EsLint` to `ESLint Stylistic`](https://eslint.org/blog/2023/10/deprecating-formatting-rules)
- New Rules enabled:
  - no-object-constructor
- Deprecated rules removed:
  - no-return-await
  - no-new-object
- Rules changed:
  - lines-between-class-members
    - Set `enforce` option with certain style

## v5.2.0 (2023-02-06):

- Bump eslint dependency to >= 8.33.0
- Bump eslint-plugin-import dependency to 2.26
- Rule changes:
	- line-comment-position
		- Change option name from `applyDefaultPatterns` to `applyDefaultIgnorePatterns`
	- no-fallthrough
		- Set option `allowEmptyCase: false`
- New Rules enabled:
	- logical-assignment-operators (warning)
	- no-new-native-nonconstructor
- Deprecated rules removed:
	- id-blacklist

## v5.1.0 (2022-08-16):

- Bump eslint dependency to >= 8.19.0
- New rules enabled:
	- no-constant-binary-expression
- Rules add secondary option and set value same default:
	- no-confusing-arrow
		- Set option `onlyOneSimpleParam: false`
	- no-shadow
		- Set option `ignoreOnInitialization: false`
	- no-use-before-define
		- Set option `functions: true`
		- Set option `classes: true`
		- Set option `variables: true`
		- Set option `allowNamedExports: false`

## v5.0.0 (2022-01-21):

- Migrate eslint dependency to version 8
- Bump eslint dependency to >= 8.7.0
- Bump eslint-plugin-import dependency to 2.25.4
- Rule changes:
	- object-curly-newline
		- Enabled with option `multiline: true`
- New rules enabled:
	- no-unused-private-class-members

## v4.1.1 (2021-10-07):

- Rule changes:
	- require-atomic-updates
		- Off this rule
	- spaced-comment
		- Line option allow slash for TS comments `///`
		- Block option allow `:` and `::`

## v4.1.0 (2021-05-18):

- Bump eslint-plugin-import dependency to ^2.23.2
- Rule changes:
	- import/no-duplicates
		- Add option `considerQueryString: true`

## v4.0.1 (2021-04-28):

- Fix warning to warn

## v4.0.0 (2021-04-28):

- Migrate to ESLint version 7
- Bump ESLint dependency to >= 7.24.0
- New dependency on eslint-plugin-node ^11.1.0
- Rule changes:
    - quotes
        - Change option from `double` to `single`
    - comma-dangle
        - Change option from `never` to `always-multiline`
    - computed-property-spacing
        - Add option `enforceForClassMembers: true`
    - use-isnan
        - Add option `enforceForSwitchCase: true`
        - Add option `enforceForIndexOf: false`
    - no-unsafe-negation
        - Add option `enforceForOrderingRelation: false`
    - no-unused-expressions
        - Add option `enforceForJSX: false`
    - no-sequences
        - Add option `allowInParentheses: true`
- New Rules enabled:
    - function-call-argument-newline
    - no-import-assign
    - prefer-regex-literals
    - default-param-last (warning)
    - grouped-accessor-pairs
    - no-constructor-return
    - no-dupe-else-if
    - no-setter-return
    - default-case-last
    - no-loss-of-precision (warning)
    - no-promise-executor-return
    - no-nonoctal-decimal-escape
    - no-unsafe-optional-chaining
	- node/no-new-require
	- node/no-path-concat
- Deprecated rules removed:
	- no-new-require
	- no-path-concat

## v3.0.0 (2019-04-29):

- Migrate to ESLint version 5
- Bump ESLint dependency to >= 5.16.0
- Bump eslint-plugin-import dependency to ^2.17.2
- Change "globals" config to the new syntax in ESLint v5.13
- Rule changes:
	- object-curly-newline
		- Enabled with option `consistent: true`
	- no-unused-vars:
		- Add option `ignoreRestSiblings: true`
	- no-self-assign
		- Add option `props: true`
- New rules enabled:
	- prefer-object-spread
	- max-classes-per-file
	- max-lines-per-function
	- array-element-newline
	- no-misleading-character-class
	- require-atomic-updates
	- no-async-promise-executor
	- no-useless-catch
- Deprecated rules removed:
	- no-catch-shadow
	- require-jsdoc
	- valid-jsdoc

## v2.3.0 (2018-11-16):

- Fix ecmaFeatures.experimentalObjectRestSpread warning message

## v2.2.0 (2018-01-15):

- Bump ESLint dependency to ^4.15.0
- Bump eslint-plugin-import dependency to ^2.8.0
- Add following rules:
	- implicit-arrow-linebreak
	- lines-between-class-members
	- multiline-comment-style

## v2.1.1 (2017-10-05):

- Bump ESLint dependency to ^4.8.0

## v2.1.0 (2017-09-07):

- Bump ESLint dependency to ^4.6.1
- Add "function-paren-newline" rule

## v2.0.1 (2017-08-29):

- Turn "import/no-anonymous-default-export" off

## v2.0.0 (2017-08-29):

- This is exactly the same as v1.1.0
- Old changelog message:
	- Migrate to ESLint version 4
	- New rules enabled:
		- for-direction
		- getter-return
		- nonblock-statement-body-position
		- padding-line-between-statements
		- prefer-promise-reject-errors
		- semi-style
		- template-tag-spacing
	- Rules disabled:
		- arrow-body-style
		- All deprecated rules

## v1.1.1 (2017-08-29):

- Reverse all changes in v1.1.0
- This is a hotfix as bumping ESLint to v4 is breaking change. Therefore it should be published as v2 instead of v1.1

## v1.1.0 (2017-08-28):

- Version deleted

## v1.0.3 (2017-06-08):

- Fix ESLint throw unexpected token error for spread operator
- Add Changelog
