```
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMmhhhhmMMMMMMNhhhhdMMMMMMMNhhhhhNMMMMMMMMMNNdhhhmNMMMMMMdhhhhhhhhhhhhhmMM
MMo.....sNMMMh-....:MMMMMMM+.....+MMMMMMMmo:......-/yNMMM:.............oMM
MMo......:hN+......:MMMMMMs.......yMMMMMs....-::-....:dMM/.....::::::::sMM
MMo....-...//--....:MMMMMh....-....hMMMy....omMMNd/...-NMNo-...oNMMMMMMMMM
MMo....h+---:yd....:MMMMd-...:d-...-mMM/...-MMMMMMd....hMMMy/.../NMMMMMMMM
MMo....mMmmmmMM....:MMMm-....dMh....:NMy....smMMNd/...-NMNs-:+:oNMMMMMMMMM
MMo....mMMMMMMM....:MMN/....-ho/.....+MMo....-/oo/:..:dMM/....:////////sMM
MMo....mMMMMMMM....:MM+....-sNNy-.....sMMdo:.....--/+dMMM:.............oMM
MMdhhhhNMMMMMMMhhhhdMNhhhhhmMMMMNhhhhhhMMMMNmy+:.....:dMMdhhhhhhhhhhhhhdMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNho:oNMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

```

# eslint-config-maqe

This package provides MAQE's .eslintrc as an extensible shared config.

---

## Usage

### Prerequisites
- Make sure you upgrade to at least Node.js 12.22.0, 14.7.0, or 16.0.0 when using eslint-config-maqe v5.0.0 because eslint-config-maqe v5.0.0 depend on eslint v8.0.0 [see Node.js 10, 13, and 15 are no longer supported](https://eslint.org/docs/user-guide/migrating-to-8.0.0#drop-old-node)
- If you are unable to upgrade, We recommend use eslint-config-maqe version 4 until you are able to upgrade Node.js

### Using MAQE Boilerplate

If you're using [MAQE Boilerplate](https://bitbucket.org/maqe/mqbp), then you don't have to do anything! MAQE Boilerplate already has this package as its dependency. Sit back and enjoy!

### Using without MAQE Boilerplate

If you don't use [MAQE Boilerplate](https://bitbucket.org/maqe/mqbp), you'll need to do the following:

- Install all dependencies
```
npm install --save-dev eslint
npm install --save-dev eslint-plugin-import
npm install --save-dev eslint-plugin-node
npm install --save-dev @stylistic/eslint-plugin@1.5.3
```

- Install the package
```
npm install --save-dev eslint-config-maqe
```

- Create your .eslintrc file and extend it from this config
```json
{
    "extends": "maqe"
}
```
See [ESLint Documentation](http://eslint.org/docs/user-guide/configuring#extending-configuration-files) for more information.

---

## Override ESLint config

You can modify your .eslintrc in your project as you want. Some common overrides are:

### Globals

You can add global variables in your project by adding "globals" in your .eslintrc

```json
{
    "globals": {
        "enquire": "writable",
        "FontFaceObserver": "writable",
        "imagesloaded": "writable",
        "Modernizr": "writable"
    }
}
```

See [Specifying Globals](http://eslint.org/docs/user-guide/configuring#specifying-globals) for more information.

### Rules

You can override the default rules by adding "rules" in your .eslintrc

```json
{
    "rules": {
        "eqeqeq": "off",
        "curly": "error",
        "quotes": ["error", "double"]
    }
}
```

See [Configuring Rules](http://eslint.org/docs/user-guide/configuring#configuring-rules) for more information.

See [Rules](http://eslint.org/docs/rules/) for list of ESLint rules.

---

## Migration to version 6.1.0

### Replaced Deprecated Stylistic Rules in ESLint with @stylistic/eslint-plugin Plugin
Since the ESLint v8.53.0 update, 67 stylistic rules have been deprecated.
To adhere to the updated guidelines, we have replaced the deprecated rules with the `@stylistic/eslint-plugin` plugin, which now handles stylistic linting.

Install new dependency packages.
```
npm install --save-dev eslint@8.56
npm install --save-dev eslint-plugin-import@2.29.1
npm install --save-dev @stylistic/eslint-plugin@1.5.3
```

You can customize specific rules by prefixing them with `@stylistic/`.
Here is an example configuration in your stylelint configuration file:

```json
{
    "extends": "eslint-config-maqe",
    "rules": {
        // stylistic rules from @stylistic/eslint-plugin:
        "@stylistic/indent": ["error", "tab"],
    }
}
```

Feel free to modify and extend the rules based on your project requirements. This ensures your code adheres to the latest best practices and maintains a consistent and clean code style.